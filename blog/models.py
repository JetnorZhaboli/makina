from django.db import models
# from django.db.models import Manager
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=255, null=False)
    content = models.TextField(null=False)
    photo = models.ImageField(upload_to="posts", null=True, blank=True)

    # stuff = Manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={'pk': self.id})


