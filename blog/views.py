from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from blog.models import Post
from blog.forms import PostForm, PostForm2


def get_post_list(request):
    visits = request.session.get('bloglistvisits', 0)

    the_posts = Post.objects.all()

    request.session['bloglistvisits'] = visits + 1
    request.session.modified = True # force cookie to be updated

    return render(request, "blog/post_list.html", context={
        "post_list": the_posts
    })


class PostListView(ListView):
    model = Post
    # template_name = "blog/post_list.html"
    context_object_name = "posts"


def get_post_detail(request, pk):
    post = Post.objects.get(pk=pk)
    return render(request, "blog/post_detail.html", context={
        "post": post
    })


class PostDetailView(DetailView):
    model = Post
    # template_name = "blog/post_detail.html"


def add_new_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = Post(**form.cleaned_data)
            post.save()
            return redirect("post_detail", pk=post.id)
    else:
        form = PostForm()

    return render(request, "blog/post_add.html", context={
        "form": form
    })


def add_post_2(request):
    if request.method == "POST":
        form = PostForm2(request.POST)
        if form.is_valid():
            post = form.save()
            return redirect("post_detail", pk=post.id)
        else:
            print("is not valid")
    else:
        form = PostForm2()

    return render(request, "blog/post_add.html", context={
        "form": form
    })

# from django.views.generic import CreateView
class PostCreateView(CreateView):
    model = Post
    fields = ['title', 'content']


def about_us(request):
    return render(request, "blog/about.html")


class AboutView(TemplateView):
    template_name = "blog/about.html"


class PostUpdateView(UpdateView):
    model = Post
    fields = '__all__'  # ['title', 'content', 'photo']


class PostDeleteView(DeleteView):
    model = Post
    success_url = reverse_lazy('post_list')
