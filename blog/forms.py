from django import forms
from django.forms import widgets

from blog.models import Post

class PostForm(forms.Form):
    title = forms.CharField(max_length=255)
    content = forms.CharField(widget=widgets.Textarea)

class PostForm2(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'content']

