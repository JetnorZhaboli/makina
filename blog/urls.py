from django.urls import path

from blog import views


urlpatterns = [
    path("", views.get_post_list, name="post_list"),
    path("list", views.PostListView.as_view(), name="post_list2"),
    path("add/", views.add_new_post, name="post_add"),
    path("new", views.PostCreateView.as_view(), name="post_new"),
    path("<int:pk>", views.get_post_detail, name="post_detail"),
    path("<int:pk>/details", views.PostDetailView.as_view(), name="post_detail2"),
    path("<int:pk>/edit", views.PostUpdateView.as_view(), name="post_edit"),
    path("<int:pk>/delete", views.PostDeleteView.as_view(), name="post_delete"),
    path("about", views.about_us, name="about_us"),
    path("about2", views.AboutView.as_view(), name="about_us2"),
]
