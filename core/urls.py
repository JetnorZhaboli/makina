from django.urls import path

from core.views import HomeView, save_preferences


urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("save-preferences", save_preferences, name="save_preferences")
]
