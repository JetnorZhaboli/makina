from django.shortcuts import redirect
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "home.html"


def save_preferences(request):
    print("SAVE PREFERENCES::request.POST =", request.POST)

    theme = request.POST.get("theme", "light")

    request.session['theme'] = theme

    return redirect("/")
