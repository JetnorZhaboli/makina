from django.contrib import admin
from vehicles.models import Vehicle

# Register your models here.

class VehicleAdmin(admin.ModelAdmin):
    pass

admin.site.register(Vehicle, VehicleAdmin)

