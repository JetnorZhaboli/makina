from django.urls import path, include
from rest_framework import routers

from vehicles.views import (
    add_vehicle,
    get_vehicles,
    get_vehicle_details,
    put_vehicle_on_sale,
    show_secret_page,
)
from vehicles.viewsets import VehicleViewSet


router = routers.DefaultRouter()
router.register('vehicles', VehicleViewSet)


urlpatterns = [
    path("", get_vehicles, name="vehicle_list"),
    path("api/", include(router.urls)),
    path("super-secret/", show_secret_page, name="secret_page"),
    path("add/", add_vehicle, name="vehicle_add"),
    path("<int:pk>/", get_vehicle_details, name="vehicle_detail"),
    path("<int:pk>/put-on-sale/", put_vehicle_on_sale, name="vehicle_sale"),
]
