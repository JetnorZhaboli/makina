from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test

from vehicles.models import Vehicle
from vehicles.forms import VehicleForm


# 127.0.0.1:8000/vehicles
def get_vehicles(request):
    # select * from vehicles_vehicle
    vehicles = Vehicle.objects.all()
    return render(request, "vehicles/vehicle_list.html", context={
        "vehicles": vehicles
    })


# 127.0.0.1:8000/vehicles/2
def get_vehicle_details(request, pk):
    # select * from vehicles_vehicle where id = %s
    vehicle = Vehicle.objects.get(pk=pk)
    return render(request, "vehicles/vehicle_detail.html", context={
        "vehicle": vehicle
    })

@login_required
@permission_required('vehicles.can_put_on_sale', raise_exception=True)
def put_vehicle_on_sale(request, pk):
    if request.method == 'POST':
        vehicle = Vehicle.objects.get(pk=pk)
        vehicle.is_on_sale = True
        vehicle.save()
        return redirect('vehicle_detail', pk=pk)


@login_required
def add_vehicle(request):
    if request.method == 'POST':
        form = VehicleForm(request.POST)
        if form.is_valid():
            vehicle = form.save()
            return redirect('vehicle_detail', pk=vehicle.pk)
    else:
        form = VehicleForm()
    return render(request, "vehicles/vehicle_form.html", context={
        'form': form
    })


def is_sdacademy_dev(user):
    return user.email.endswith('@sdacademy.dev')


@user_passes_test(is_sdacademy_dev)
def show_secret_page(request):
    return render(request, 'vehicles/secret.html')

