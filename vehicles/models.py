from django.db import models
from django.contrib.auth.models import User


class Merchant(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, blank=False)
    name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name


# CREATE TABLE vehicles ()
class Vehicle(models.Model):
    AUTOMATIC = "automatic"
    MANUAL = "manual"
    TRANSMISSION_CHOICES = [
        (AUTOMATIC, "Automatic"),
        (MANUAL, "Manual"),
    ]

    # brand varchar(100) not null
    brand = models.CharField(max_length=100, null=False)
    # brand varchar(100) not null
    model = models.CharField(max_length=100, null=False)
    # year int not null constraint chk_positive check (year > 0)
    year = models.PositiveIntegerField(null=False)
    transmission = models.CharField(max_length=32, null=True,
                                    choices=TRANSMISSION_CHOICES)
    description = models.TextField(null=True, blank=True)
    merchant = models.ForeignKey(Merchant,
                                 null=True, blank=False,
                                 on_delete=models.CASCADE, related_name="vehicles")
    is_on_sale = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ('can_put_on_sale', 'Put vehicle on sale'),
        )

    def __str__(self):
        return f'{self.brand} {self.model}'


class Photo(models.Model):
    photo = models.ImageField(null=True, upload_to="photos")
    vehicle = models.ForeignKey(Vehicle, null=False, blank=False,
                                related_name='photos', on_delete=models.CASCADE)

    def __str__(self):
        return self.photo.name


class Comment(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    comment = models.TextField(null=False, blank=False)
    vehicle = models.ForeignKey(Vehicle, null=False, blank=False,
                                related_name="comments", on_delete=models.CASCADE)

    def __str__(self):
        return self.name
